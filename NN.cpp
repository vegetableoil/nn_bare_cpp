#include <iostream>
#include <cstdio>
#include <vector>
#include <cmath>
#include <cstdarg>
#include <random>
#include <limits>

using namespace std;

		std::random_device gen;
		std::uniform_real_distribution<double> d_normal(-1.0,1.0);
		//std::normal_distribution<double> d_normal(0,0.00001);

const double EPS = 0.000001;

int dist(int a, int b, int x, int y, int dist) {
	if ((abs(a-x)<=dist) && (abs(b-y))<=dist) return 1;
	return 0;
}

bool is_nan(double a) {
	return (!(a > 0) && !(a < 0) && !(a == 0));
}

bool eq(double a, double b) {
	return abs(a-b)<EPS;
}

int one(double a) {
	/*if (abs(a) < EPS) {
		return 0;
	} else */
	if (a > 0) 
		return 1;
	else 
		return -1;
	
}

class Vector { 
public:
	int n;
	vector<double> v;

	double mean() const {
		double sum1 = 0;
		for (int i = 0; i < n; i++) {
			sum1 += abs(v[i]);
		}
		return sum1/n;
	}

	Vector(int n0): n(n0)
	{
		v = vector<double>(n,0);
	}
	Vector(int n0, int a0): n(n0)
	{	
		v = vector<double>(n,a0);
	}		
	double& operator[](int i) {
		if (i < 0 || i >= n ) throw;
		return v[i];
	}
	const double operator[](int i) const{
		if (i < 0 || i >= n ) throw;
				//cerr << "n: " << n << endl;
		return v[i];
	}
	Vector& operator-=(const Vector& x) {
		for (int i = 0; i < min(n,x.n); i++) {
			v[i] -= x[i];
		}
	}
	Vector operator+(const Vector& x) {
		Vector res(n);
		for (int i = 0; i < min(n,x.n); i++) {
			res[i] = v[i] + x[i];
		}
		return res;
	}
	Vector operator-(const Vector& x) {
		Vector res(n);
		for (int i = 0; i < min(n,x.n); i++) {
			res[i] = v[i] - x[i];
		}
		return res;
	}
	Vector operator*(double a) {
		Vector res(n);
		for (int i = 0; i < n; i++) {
			res[i] = v[i]*a;
		}
		return res;
	}
	friend Vector operator*(double a, const Vector& v) {
		Vector res(v.n);
		for (int i = 0; i < v.n; i++) {
			res[i] = v[i]*a;
		}
		return res;
	}
	void randomize(int m=-1) {
		if (m==-1) m=n;
		std::uniform_real_distribution<double> normal_v(-1./sqrt(m),1./sqrt(m));
		for (int i = 0; i < n; i++) {
			v[i] = normal_v(gen);
		}
	}

	Vector normalize() {
		double sum = 0;
		for (int i = 0; i < n; i++) {
			sum += v[i]*v[i];
		}
		sum = sqrt(sum);
		if (sum > EPS) {
			for (int i = 0; i < n; i++) {
				v[i] /= sum;
			}
		}
		return *this;
	}

	Vector normalize() const {
		Vector v1 = *this;
		double sum = 0;
		for (int i = 0; i < n; i++) {
			sum += v1[i]*v1[i];
		}
		sum = sqrt(sum);
		if (sum > EPS) {
			for (int i = 0; i < n; i++) {
				v1[i] /= sum;
			}
		}
		return v1;
	}
};


double dot(const Vector& a,const Vector& b) {
	double res = 0;
	for (int i = 0; i<a.n ; i++) {
		res += a[i]*b[i];
	}
	return res;
};

void Draw(const Vector& v) {
		for (int i = 0; i < sqrt(v.n); i++) {
			for (int j = 0; j < sqrt(v.n); j++) {
				double res = v[i + sqrt(v.n)*j];
				if (res > 0) {
					if (res >= 0.5) {
						if (res >= 0.75) cout << "█";
						if (res < 0.75) cout << "▉";
					} else if (res < 0.5) {
						if (res >= 0.25) cout << "▓";
						if (res < 0.25) cout << "▓";
					}
				} else if (res <= 0) {
					if (res <= -.5) {
						if (res <= -.75) cout << "░";
						if (res > -.75) cout << "░";
					} else if (res > -.5) {
						if (res <= -.25) cout << "▒";
						if (res > -.25) cout << "▒";						
					}					
				}
				//printf("%4.4f ",x3[i + sqrt_t*j]);
			}
			cout << endl;
		}
		cout << endl;
}

class Matrix {
public:
	int w,h;
	vector<Vector> m;
	Matrix(int h0, int w0): h(h0),w(w0)
	{
		m = vector<Vector> (h0, 
			Vector (w0));
	}
	Matrix(int n, const Vector& V): h(n), w(V.n) {
		m = vector<Vector> (n, V);
	}

	double operator()(int i, int j) const {
		return m[i][j];
	}

	double& operator()(int i, int j) {
		return m[i][j];
	}

	Vector& operator() (int i) {
		return m[i];
	}
	Vector operator() (int i) const {
		return m[i];
	}

	const Matrix operator*(double a) const {
		Matrix M = *this;
		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				M(i,j) *= a;
			}
		}
		return M;
	}

	friend Matrix operator*(double b, const Matrix& a) {
		return a*b;
	}

	friend Matrix operator*(const Matrix& A, const Matrix& B) {
		Matrix C(A.h,B.w);
		for (int i = 0; i < A.h; i++) {
			for (int j = 0; j < B.w; j++) {
				C(i,j) = 0;
			}
		}
		for (int i = 0; i < A.h; i++) {
			for (int j = 0; j < B.w; j++) {
				for (int f = 0; f < A.w; f++) {
					C(i,j) += A(i,j)*B(f,j);
				}
			}
		}
		return C;
	}

	friend Vector operator*(const Matrix& B, const Vector& a) {
		Vector v(B.h);
		for (int i = 0; i < B.h; i++) {
			v[i] = dot(B(i),a);
		}
		return v;
	}
	Matrix operator-=(const Matrix& M) {
		for (int i = 0; i < h; i++) {
			(*this)(i) = (*this)(i) - M(i);
		}	
		return *this;
	}
	Matrix operator+(const Matrix& M) {
		cerr << h << " " << M.h;
		for (int i = 0; i < h; i++) {
			(*this)(i) = (*this)(i) + M(i);
		}
		return *this;
	}

	void print() {
		cout << endl;
		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				printf("%4.4f ", m[i][j]);
			}
			cout << endl;
			cout << endl;
		}
		cout << endl;
	}
	void randomize(int n = -1) {
		if (n==-1) n=w;
	std::uniform_real_distribution<double> normal_M(-1.0/sqrt(n),1.0/sqrt(n));
		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				m[i][j] = normal_M(gen);
			}
		}
		/*double sum = 0;

		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				sum += m[i][j]*m[i][j];
			}
		}

		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				m[i][j] *= 1/sqrt(sum);
			}
		}
*/
		//cerr <<m[0][0];
	}
	double weight() {
		double sum = 0;
		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				sum += m[i][j]*m[i][j];
			}
		}
		sum /= w*h;
		return sum;
	}
};

double f_(double x);

double f(double x) {
	return 2 / (1 + exp(-x) ) - 1;
}
/*
void __ERR(double a, double b) {
	cerr << endl << endl << "In f" << endl;
	cerr << "x = " << a << endl;
	cerr << "Out f" << endl;	
}*/

double f_(double x) {
	/*cerr << endl << endl << "In f_" << endl;
	cerr << "x = " << x << endl;
	cerr << "f_ = " << exp(-x) /( (1 + exp(-x))*(1 + exp(-x)) ) << endl;
	cerr << "Out f_" << endl;*/
	return 2*f(x)*(1-f(x));
}

float relu(float x) {
	return x>0?x:0;
}
float relu_(float x) {
	return x>0?1:0;
}

float dtanhf(float a) {
	return 1 - tanhf(a)*tanhf(a);
}

double f1(double x) {return tanhf(x);}
double f2(double x) {return tanhf(x);}
double f3(double x) {return tanhf(x);}
double f4(double x) {return tanhf(x);}

double f1_(double x) {return 1 - tanhf(x)*tanhf(x);}
double f2_(double x) {return 1 - tanhf(x)*tanhf(x);}
double f3_(double x) {return 1 - tanhf(x)*tanhf(x);}
double f4_(double x) {return 1 - tanhf(x)*tanhf(x);}

double ced(double z, double y0) {
	// L = -[y0*log(z) + (1-y0)*(log(1-z))]
	double res = -(y0/(z+EPS) - (1-y0)/(1-z+EPS));
	return z-y0;//=res;
}

/*double cedv(const Vector& a, const Vector& b) {
	double sum = 0;
	return 0;
}
*/

double logreg(double z, double y0) {
	return -(y0*log(z+EPS) + (1-y0)*(log(1-z+EPS)));
}

double dot(const Matrix& M, int J, const Vector& x)	 {
	double sum = 0; //cerr << endl;
	for (int i = 0; i < M.w; i++) { //cerr << "i" << i ;
		//cerr << "M( " << J << " " << i << ") = " << M(J,i);
		//cerr << "x[ " << i  << "] = " << x[i];
		sum += M(J,i)*x[i];
	}
			//cerr << endl << "sum " << sum << endl << endl;
	//cerr << endl;	
	return sum;
}


Vector app(double(*h) (double), const Vector& a) {
	Vector v = a;
	for (int i = 0; i < a.n; i++){
		v[i] = h(v[i]);
	}
	return v;
}

Vector app(double(*h) (double,double), const Vector& a, const Vector& b) {
	Vector v = a;
	for (int i = 0; i < a.n; i++){
		v[i] = h(a[i],b[i]);
	}
	return v;
}
/*
template<class T>
Vector app(double(*h) (...), std::initializer_list<T> list) 
{
	static_assert(list.size() > 0, "Number of vectors must not be zero or neg.");
	Vector v = list.begin();

}
*//**
Vector app(double(*h) (double), ...) {
	va_list args;
	va_start(args,h);

}
*/
Matrix mul(const Vector& V, const Matrix& A) {
	Matrix a = A; cerr << "in";
	for (int i = 0; i < V.n; i++) {
		a(i) = a(i)*V[i]; cerr << i;
	}cerr << "out";
	return a;
}
	std::normal_distribution<float> offset(0,4);
int ofst() {
	return (int)abs(offset(gen));
}

void Gen_Ex(Vector& Map, int size, int* clas, int one, bool norm=false) {
	//int size = s;
	std::uniform_real_distribution<float> d(-1,1);
	std::normal_distribution<float> normal(0,0.5);
	double margin = 1.1;min( 3.5, abs(normal(gen))+1.1 ); 
	bool done = false; float dgen = d(gen);
	do {
	if ( dgen > 0) { *clas = 1; cout << "1";
		double phi = d(gen)*M_PI;
		double r = (size/2.)*(normal(gen)+1)/2.;
		double rx = r*cos(phi);
		double ry = r*sin(phi);
		int i0 = ofst(); int j0 = ofst(); int i1 = ofst(); int j1 = ofst();
		for (int i = i0; i < size-i1; i++) {
			for (int j = j0; j < size-j1; j++) {
				if (sqrt(abs(rx*(i-size/2) + ry*(j-size/2) - r*r)) < 0.7*margin*sqrt(r)) { 
					Map[i +size*j] = one;
					done = 1;
				} 
			}
		}
	} else { *clas = -1; cout << "0";
		double R = ( (size/2. - 2)*(d(gen)+1)/2 + 2 );
		double x = d(gen)*(size/2.0 - R - 1)+size/2;
		double y = d(gen)*(size/2.0 - R - 1)+size/2;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (abs((R*R - (x-i)*(x-i) - (y-j)*(y-j))) < margin*R) {
					Map[i+size*j] = one;
					done = 1;
				} 
			}
		}
	}
	} while(!done);

	if (norm) {
		double sum = 0;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			sum += Map[i+size*j]*Map[i+size*j]; 
		}
	}		

	sum = sqrt(sum);
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			Map[i+size*j] /= sum; 
		}
	}		

	}
	//return Map;
};

class Layer {
public:
	double delta, alpha;
	bool has_bias;

	float (*activation) (float);
	float (*dactivation) (float);

	virtual Vector forward(const Vector&) {};
	virtual Vector backward(const Vector&) {};

	Layer(bool has_bias,
		double delta, double alpha, float (*act) (float), float(*dact) (float)):
		delta(delta),
		alpha(alpha),
		activation(act),
		dactivation(dact)
	{
	}

	virtual ~Layer() {}
};

class Sequential: public Layer {
public:
	Matrix W;
	Vector lastx, last_res, f_last_res, fdiv_last_res, bias;
	int dist0, input_dim_w, input_dim_h;
	int output_dim_w, output_dim_h;


	Sequential(int input_dim, int output_dim, bool has_bias=true,
		double delta=0.1, float (*act) (float) =relu, float (*dact) (float)=relu_, double alpha=0):
		lastx(input_dim, 0),
		last_res(output_dim, 0),
		f_last_res(output_dim, 0),
		fdiv_last_res(output_dim, 0),
		W(output_dim,input_dim),
		bias(output_dim, 0),
		dist0(-1),
		Layer(has_bias,delta,alpha, act, dact) 
	{ 		W.randomize();
		if (has_bias) {
			bias.randomize();
		}
	}

	Sequential(const char* d2, int input_dim_w, int input_dim_h, int output_dim_w, int output_dim_h, bool has_bias=true,
		double delta=0.1, float (*act) (float) =relu, float (*dact) (float)=relu_, double alpha=0, int dist=-1):
		lastx(input_dim_w*input_dim_h, 0),
		input_dim_h(input_dim_h), input_dim_w(input_dim_w),
		output_dim_h(output_dim_h), output_dim_w(output_dim_w),
		last_res(output_dim_w*output_dim_h, 0),
		f_last_res(output_dim_w*output_dim_h, 0),
		fdiv_last_res(output_dim_w*output_dim_h, 0),
		W(output_dim_w*output_dim_h,input_dim_w*input_dim_h),
		bias(output_dim_w*output_dim_h, 0),
		dist0(dist),
		Layer(has_bias,delta,alpha, act, dact) 
	{ 		W.randomize(dist*dist);
		if (has_bias) {
			bias.randomize(100000*dist);
		}
	}


	Vector forward(const Vector& x) {
		///cerr << W.weight()<<"WEIGHT";
		lastx = x; 
		if (dist0 >= 0) {
			last_res = Vector(last_res.n, 0);
			for (int i = 0; i < output_dim_h; i++) {
			for (int j = 0; j < output_dim_w; j++) {
					for (int x1 = 0; x1 < input_dim_w; x1++) {
					for (int y1 = 0; y1 < input_dim_h; y1++) {
							if (dist(i,j,x1,y1,dist0)) {
							last_res[i*output_dim_w + j] += 
								W(i*output_dim_w + j,x1*input_dim_h + y1)*x[x1*input_dim_h + y1]
								+ bias[i*output_dim_w + j];
							}						
						}
					}

				}
			}
		} else {
			last_res = W*x + bias;}
		for (int i = 0; i < last_res.n; i++) {
			f_last_res[i] = activation(last_res[i]); 
			fdiv_last_res[i] = dactivation(last_res[i]);
		}
		return f_last_res; 
	}

	Vector backward(const Vector& errors) { // cerr << " errors.n = " << errors.n;
	//cerr << "W.h" << W.h;
		Vector out_errors(W.w, 0); cerr << "out_errors" << delta*errors.mean()/*errors[0] */<< endl;
		if (dist0 >= 0) {

			for (int i = 0; i < output_dim_h; i++) {
			for (int j = 0; j < output_dim_w; j++) {
					for (int x1 = 0; x1 < input_dim_w; x1++) {
					for (int y1 = 0; y1 < input_dim_h; y1++) {		
							if (dist(i, j, x1, y1, dist0)) {
								out_errors[x1*input_dim_h + y1] += W(i*output_dim_w + j,x1*input_dim_h + y1)
								*fdiv_last_res[i*output_dim_w + j]*
								errors[i*output_dim_w + j];
							}
						}
					}

				}
			}
/*
			for (int i = 0; i < W.h; i++) {
				//double fdiv_last_res_i_errors_i = ;
				for (int j = 0; j < W.w; j++) {
					if (dist(i / input_dim_h, i % input_dim_h, j / output_dim_w, j % output_dim_w, dist0)) {
						out_errors[j] += W(i,j)*fdiv_last_res[i]*errors[i];
					}
				}
			} //cerr << "B";*/
			for (int i = 0; i < output_dim_h; i++) {
			for (int j = 0; j < output_dim_w; j++) {
					for (int x1 = 0; x1 < input_dim_w; x1++) {
					for (int y1 = 0; y1 < input_dim_h; y1++) {		
							if (dist(i, j, x1, y1, dist0)) {
								W(i*output_dim_w + j,x1*input_dim_h + y1) -= delta
									*lastx[x1*input_dim_h + y1]
									*fdiv_last_res[i*output_dim_w + j]
									*errors[i*output_dim_w + j] 
									+ alpha*W(i*output_dim_w + j,x1*input_dim_h + y1);
							}
						}
					}
					bias[i*output_dim_w + j] -= delta*fdiv_last_res[i*output_dim_w + j]*
						errors[i*output_dim_w + j] 
						+ alpha*bias[i*output_dim_w + j];
				}
			}
			return out_errors;
/*
			for (int i = 0; i < W.h; i++) { //cerr << "1";
				//double d_fdiv_erri = ;
				for (int j = 0; j < W.w; j++) {
					if (dist(i / input_dim_h, i % input_dim_h, j / output_dim_w, j % output_dim_w, dist0)) {
					W(i,j) -= delta*lastx[j]*fdiv_last_res[i]*errors[i] + alpha*W(i,j);
					}
				} //cerr << "bias";
				bias[i] -= delta*fdiv_last_res[i]*errors[i] 
				+ alpha*bias[i];
			} 
			return out_errors;*/
		} else {
			for (int i = 0; i < W.h; i++) {
				double fdiv_last_res_i_errors_i = fdiv_last_res[i]*errors[i];
				for (int j = 0; j < W.w; j++) {
					out_errors[j] += W(i,j)*fdiv_last_res_i_errors_i;
				}
			//} //cerr << "B";

			//for (int i = 0; i < W.h; i++) { //cerr << "1";
				double d_fdiv_erri = fdiv_last_res[i]*errors[i];
				for (int j = 0; j < W.w; j++) {
					W(i,j) -= delta*lastx[j]*d_fdiv_erri + alpha*W(i,j);
				} //cerr << "bias";
				bias[i] -= delta*d_fdiv_erri 
				+ alpha*bias[i];
			} 
			return out_errors;
		}
	}

	~Sequential() {}

};

class Convo: public Layer {
public:
	vector<Vector  > Ww;
	Vector bias;
	Vector lastx, fdiv_last_res;
	int X,Y,Z,XY,frameW,frameH,frameD, ywidth,yheight,ydepth;
//	int frameW_2, frameH_2, frameD_2;

	Convo(int X0, int Y0, int Z0,
		  int W, int H,int D,
		  int ywi, int yhe, int yde, double delta, float (*act) (float)=tanhf, float (*dact) (float)=dtanhf, double alpha=0):
			X(X0), Y(Y0), Z(Z0), frameW(W), frameH(H), frameD(D),
			ywidth(ywi), yheight(yhe), ydepth(yde),	
		   Ww(ydepth, Vector(frameW*frameH*frameD,0)), lastx(X*Y*Z), 
		   bias(ydepth), XY(X*Y), 
	fdiv_last_res(ywidth*yheight*ydepth),
	Layer (true, delta, alpha, act, dact){ 
		Ww = vector<Vector>(ydepth, Vector(frameW*frameH*frameD,0));
		fdiv_last_res = Vector(ywidth*yheight*ydepth, 0),
		bias = Vector(ydepth, 0); 
		for (int i = 0; i < ydepth; i++) {
			Ww[i].randomize(); 
		}
		bias.randomize();

	}

	Vector forward(const Vector& x1) { 
		lastx = x1.normalize();
		Vector result(ywidth*yheight * ydepth );
		for (int i = 0; i < ywidth; i++) {
			for (int j = 0; j < yheight; j++) {
				for (int k = 0; k < ydepth; k++) {
					double sum = 0;
					for (int x = -frameW/2; x < frameW/2 + frameW%2; x++) {
					for (int y = -frameH/2; y < frameH/2 + frameH%2; y++) {
					for (int z = -frameD/2; z < frameD/2 + frameD%2; z++) {
						sum += lastx[-ywidth/2 + X/2+ i + x
								 + X*(-yheight/2 +Y/2 + j + y)
								 + XY*(Z/2 + z)]
								*Ww[k][x+frameW/2 + 
									frameW*(y+frameH/2) + 
									frameW*frameH*(z+frameD/2)];
					}
					}}
					sum += bias[k];
					result[i + ywidth*j + ywidth*yheight*k] = activation(sum);
					fdiv_last_res[i + ywidth*j + ywidth*yheight*k] += dactivation(sum) ;
				}
			}
		}//cerr << "A" << endl;
		return result;
	}

	Vector backward(const Vector& errors) {  
		Vector out_errors(XY*Z,0); cerr << "out_errors" << delta*errors.mean() << endl;
		for (int i = 0; i < ywidth; i++) {
			for (int j = 0; j < yheight; j++) {
				for (int k = 0; k < ydepth; k++) {
					double fdiv_last_res_i_errors_i
					 = fdiv_last_res[i + ywidth*j + ywidth*yheight*k]
					 	*errors[i + ywidth*j + ywidth*yheight*k];	
					for (int x = -frameW/2; x < frameW/2 + frameW%2; x++) {
					for (int y = -frameH/2; y < frameH/2 + frameH%2; y++) {
					for (int z = -frameD/2; z < frameD/2 + frameD%2; z++) {
						out_errors[-ywidth/2 + X/2 + i + x
								 + X*(-yheight/2 + Y/2+ j + y)
								 + XY*(/*+ k*/Z/2 + z)] += 
							Ww[k][x+frameW/2 + 
									frameW*(y+frameH/2) + 
									frameW*frameH*(z+frameD/2)]*
							fdiv_last_res_i_errors_i / (ywidth*yheight*ydepth);
					}
					}}
				}
			}
		}		
		for (int i = 0; i < ywidth; i++) {
			for (int j = 0; j < yheight; j++) {
				for (int k = 0; k < ydepth; k++) {
					double d_fdiv_erri = fdiv_last_res[i + ywidth*j + ywidth*yheight*k]
						*errors[i + ywidth*j + ywidth*yheight*k];
					for (int x = -frameW/2; x < frameW/2 + frameW%2; x++) {
					for (int y = -frameH/2; y < frameH/2 + frameH%2; y++) {
					for (int z = -frameD/2; z < frameD/2 + frameD%2; z++) {
							Ww[k][x+frameW/2 + 
									frameW*(y+frameH/2) + 
									frameW*frameH*(z+frameD/2)] -= 
							(delta*lastx[-ywidth/2 + X/2+ i + x
								 + X*(-yheight/2 +Y/2 + j + y)
								 + XY*(Z/2 + z)]*d_fdiv_erri 
							+ alpha*Ww[k][x+frameW/2 + 
									frameW*(y+frameH/2) + 
									frameW*frameH*(z+frameD/2)]);
					}
					}}
					bias[k] -= delta*d_fdiv_erri + alpha*bias[k];
				}
			}
		}	// cerr << "H";
		return out_errors;	
	}
	~Convo() {}
};

/*
class Convolutional: public Layer
	{
public:
	double input_area, input_dim, output_dim;

	static double prod(const vector<int>& v) {
		int product = 1;
		for (auto a: v) {
			product *= a;
		}
		return product;
	}

	Convolutional(const vector<int>& v_input_area, 
					const vector<int>& v_input_dim, 
					const vector<int>& v_output_dim, bool has_bias=true,
					double delta=0.1, double alpha=0):
		input_area(prod(v_input_area)),
		input_dim(prod(v_input_dim)), 
		output_dim(prod(v_output_dim)),
		Layer() 
	{
		W = Matrix(output_dim, input_area);
		bias = Vector(output_dim);
	}

	Vector forward(const Vector & x) {
		lastx = x;
		for (vector<int> pos_out = v_output_dim; 
			 less(pos_out, v_output_dim),
			 inc(pos_out))
		{
			for (int i = 0; i < output_area; i++) {
				
			}
		}
	}
};*/

class NN {
public:
	vector<Layer*> Layers;
	float (*act) (float);
	float (*dact) (float);

	double(*loss) (double,double);

	NN(double(*loss)(double, double)=ced, float(*defact) (float)=relu, float(*dfact) (float)=relu):
	act(defact), dact(dfact) {}

	~NN () {
		for (auto i: Layers) {
			delete i;
		}
	}

	struct args__addSeq {
		int input_dim;
		int output_dim;
		bool has_bias=true;
		double delta=0.1;
		float(*f) (float)=NULL;
		float(*df) (float)=NULL;
		double alpha=0;
	};
	void addSeq(const args__addSeq &A) 
	{
		Layers.push_back(new Sequential(A.input_dim, A.output_dim,
			A.has_bias, A.delta,  A.f, A.df, A.alpha));
	}

	void addSeqF(const char* d2, int input_dim_w, int input_dim_h, int output_dim_w, int output_dim_h, bool has_bias=true,
		double delta=0.1, float(*f) (float)=NULL,float(*df) (float)=NULL,
		double alpha=0, int dist=-1) 
	{
		Layers.push_back(new Sequential(d2, input_dim_w, input_dim_w, output_dim_w, output_dim_h,
			has_bias, delta,  f, df, alpha, dist));
	}

	void addConv(int X0, int Y0, int Z0,
		  int W, int H,int D,
		  int ywi, int yhe, int yde, double delta, float (*f) (float)=NULL, float (*df) (float)=NULL, double alpha=0) 
	{
		if (NULL==f) {f = act;}
		if (NULL==df){df=dact;}
		Layers.push_back(new Convo(X0, Y0, Z0, W, H,D, 
			 ywi, yhe, yde, delta, f, df, alpha));
	}

	void addConvF(int X0, int Y0, int Z0,
		  int W, int H,int D,
		  int ywi, int yhe, int yde, double delta, float (*f) (float)=NULL, float (*df) (float)=NULL, double alpha=0) 
	{
		if (NULL==f) {f = tanhf;}
		if (NULL==df){df=dtanhf;}
		Layers.push_back(new Convo(X0, Y0, Z0, W, H,D, 
			 ywi, yhe, yde, delta, f, df, alpha));
	}

	void train(const Vector& input, const Vector& output) {
		Vector result = this->forward(input);


		Vector error = output; //cerr << "output.n = " << output.n; 
		for (int i = 0; i < output.n; i++) {
			error[i] = ced(result[i], output[i]);
		} 

		for (int i = Layers.size()-1; i >= 0; i--) {
			error = Layers[i]->backward(error);
			 
		}
	}

	Vector forward(const Vector& input) {
		Vector result = input;
		for (int i = 0; i < Layers.size(); i++) {
			result = Layers[i]->forward(result);
			//Draw(result);
		}
		return result;
	}

};

class RTANH {
public:
	float a, d_a;
	RTANH(const float a): a(a), d_a( (abs(a) > EPS ? 1./a : (a>0?1:-1)/EPS) ){

	}

	float operator() (float x) {
		if (x > a) return 1;
		if (-a <= x && x <= a) return d_a*x;
		if (-a > x) return -1;
	}

	float d(float x) {
		if (x > a) return 0;
		if (-a <= x && x <= a) return d_a;
		if (-a > x) return 0;		
	}
};

float rtanh(float x) {
	float a = 0.1;
	if (x > a) return 1;
	if (-a < x && x < a) return 0;
	if (-a > x) return -1;
}

int dist(int i, int j, int h, int w) {
	int x0 = i % w;
	int y0 = i / w;

	int x1 = j % h + (w-h)/2;
	int y1 = j / h + (w-h)/2;

	if (max(abs(x0-x1), abs(y0-y1)) <= (w-h)/2) {
		return 1;
	} else {
		return 0;
	}
}

float bump_f(float a) {
	return exp(-a*a);
}

float bump_df(float a) {
	return -2*a*exp(-a*a);
}


int main() {

	int sqrt_t = 21;

	NN net(ced, tanhf, dtanhf);
	//net.addConv(15,15,1,   5, 5, 1,   11,11,3,   0.9,   relu, relu_,  0.0);
	//net.addSeqF("flat", sqrt_t,sqrt_t, sqrt_t,sqrt_t, true, 0.01,  tanhf, dtanhf,  0.00, 4);
	//net.addSeq(sqrt_t*sqrt_t, 48, true, 0.2,  tanhf, dtanhf);
	//net.addSeq(100, 48, true, 0.2,  tanhf, dtanhf);
	//net.addSeq(11*11*3, 300, true, 0.5,  tanhf, dtanhf,  0.0);
	//net.addSeq(300, 200, true, 0.01, tanhf, dtanhf, 0.0);
	//net.addSeq(200, 150, true, 0.01, tanhf, dtanhf, 0.0);
	///net.addSeq(150, 50, true, 0.01, tanhf, dtanhf, 0.0);
	//net.addSeq(50, 20, true, 0.01, tanhf, dtanhf, 0.0);
	//net.addSeq(20, 1, true, 0.01, tanhf, dtanhf, 0.0);

	//net.addConv(sqrt_t, sqrt_t, 1,   3, 3, 1,   sqrt_t-2, sqrt_t-2, 2, 0.9);
	//net.addConv(sqrt_t-2, sqrt_t-2, 2,   3, 3, 2,   sqrt_t-8, sqrt_t-8, 4, 0.9);
	//net.addSeqF("flat", sqrt_t,sqrt_t, sqrt_t,sqrt_t, true, 0.02, tanhf, tanhf, 0.0, 6);
	//net.addConv(8, 8, 4,   5, 5, 1,   4, 4, 6, 0.00001);
	/*RTANH r(0.1); //*/

	net.addSeq({.input_dim=sqrt_t*sqrt_t, 
			    .output_dim=400, 
			    .has_bias=true, 
			    .delta=0.1, 
			    .f=tanhf, 
			    .df=dtanhf});
	net.addSeq({.input_dim=400, 
			    .output_dim=1, 
			    .has_bias=true, 
			    .delta=0.1, 
			    .f=tanhf, 
			    .df=dtanhf});
	//net.addSeq(100,1, true, 0.05, tanhf, dtanhf);
	//net.addSeq(128, 64, true, 0.01, tanhf, dtanhf);
	//net.addSeq(64, 1, true, 0.01, tanhf, dtanhf);

	//net.addSeq(50, 1, true, 0.2, tanhf, dtanhf);
	//net.addSeq(256, 100, true, 0.01);
	//net.addSeq(100, 1, true, 0.1);
	//net.addSeq(10*10, 1, true, 0.01);
	//net.addSeq(2*2, 1, true, 0.01);*/

for (int i = 0; i < 5000; i++) {
	int size =sqrt_t;
	cout << endl<<"Iteration: " << i << endl;
	int res1;
	Vector vec(size*size, 0);
	Gen_Ex(vec,size,&res1, 1, true);
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			if (vec[i+size*j] > 0) {
				cout << "W";
			} else {
				cout << ".";
			}
		}
		cout << endl;
	}
	cout << endl; cout <<endl<<endl;
	Vector y = Vector(1,res1);
	cerr << ".. "<< net.forward(vec)[0] << endl;
	net.train(vec,y);
	//cerr << ".. "<< net.forward(vec)[0] << endl;
}

int sum = 0;
for (int i = 0; i < 1000; i++) {
	int res1; 
	int size = sqrt_t;
	Vector vec(sqrt_t*sqrt_t, 0);
	Gen_Ex(vec,sqrt_t,&res1, 1, true);
	Vector y = Vector(1,res1);
	int c = net.forward(vec)[0]>0?1:-1;
	if (c == res1) {
		sum++;
	}else {
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			if (vec[i+size*j] > 0) {
				cout << "W";
			} else {
				cout << ".";
			}
		}
		cout << endl;
	}		
	}
}
cout << endl << endl << endl << sum/10. << " %" << endl;


exit(0);
	int m = 1;
	int n = 2*2; double sqrtn = 2;
	int k = 5*5;(sqrt_t-8)*(sqrt_t-8); double sqrtk = 5;
	int t = 10*10;(sqrt_t-4)*(sqrt_t-4); double sqrtt = 10;
	int r = sqrt_t*sqrt_t; double sqrtr = sqrt_t;
	Matrix W4(t+1,r+1), W4p(t+1,r+1);
	Matrix W3(k+1,t+1), W3p(k+1,t+1);
	Matrix W2(n+1,k+1), W2p(n+1,k+1);
	Matrix W1(m,n+1), W1p(m,n+1);

	W4.randomize();
	W3.randomize();
	W2.randomize();
	W1.randomize();

	Vector y0(m);
	Vector x1(m), f_xw1(m);
	Vector x2(n+1), f_xw2(n+1);
	Vector x3(k+1), f_xw3(k+1);
	Vector x4(t+1), f_xw4(t+1);
	Vector x5(r+1);

	double delta = 0.1;
	double l = 0.0000;
/*
	std::uniform_int_distribution<int> distribution(1,(1<<4)-1);
	for (int i = 0; i < 100; i++) {
		cout << distribution(gen) << endl;
	}
*/
	double loss,loss1,loss2; loss=loss1=loss2=1;
	for (int N = 0; N < 80007; N++) {
		cout << N;
	cout << endl;

		int res;
		std::uniform_int_distribution<int> onef(0,1);
		int one = onef(gen);
		x5 = Vector(x5.n, !one);
		Gen_Ex(x5,sqrt_t,&res, one);
		y0[0] = res;
		x5[r] = 1;

		for (int i = 0; i < t; i++) {
			f_xw4[i] = W4(i,W4.w); //bias
			for (int j = 0; j < W4.w; j++) {
				if (dist(j,i,sqrtt, sqrtr)) { 
					f_xw4[i] += W4(i,j)*x5[j];
				}
			}
			//f_xw3[i] = dot(W3,i,x4);
			x4[i] = f4(f_xw4[i]);
			f_xw4[i] = f4_(f_xw4[i]);	
			/// cerr << x3[i];

		}
		x4[t] = 1;

		cout << endl;
		for (int i = 0; i < sqrt(x4.n); i++) {
			for (int j = 0; j < sqrt(x4.n); j++) {
				double res = x4[i + sqrt(x4.n)*j];
				if (res > 0) {
					if (res >= 0.5) {
						if (res >= 0.75) cout << "█";
						if (res < 0.75) cout << "▉";
					} else if (res < 0.5) {
						if (res >= 0.25) cout << "▓";
						if (res < 0.25) cout << "▓";
					}
				} else if (res <= 0) {
					if (res <= -.5) {
						if (res <= -.75) cout << "░";
						if (res > -.75) cout << "░";
					} else if (res > -.5) {
						if (res <= -.25) cout << "▒";
						if (res > -.25) cout << "▒";						
					}					
				}
				//printf("%4.4f ",x3[i + sqrt_t*j]);
			}
			cout << endl;
		}
		cout << endl;		

		for (int i = 0; i < k; i++) {
			f_xw3[i] = W3(i,W3.w); //bias
			for (int j = 0; j < W3.w; j++) {
				if (dist(j,i,sqrtk, sqrtt)) { 
					f_xw3[i] += W3(i,j)*x4[j];
				}
			}
			//f_xw3[i] = dot(W3,i,x4);
			x3[i] = f3(f_xw3[i]);
			f_xw3[i] = f3_(f_xw3[i]);	
			/// cerr << x3[i];

		} // cerr << endl << endl; dot(W3,0,x4);

		cout << endl;
		for (int i = 0; i < sqrt(x3.n); i++) {
			for (int j = 0; j < sqrt(x3.n); j++) {
				double res = x3[i + sqrt(x3.n)*j];
				if (res > 0) {
					if (res >= 0.5) {
						if (res >= 0.75) cout << "█";
						if (res < 0.75) cout << "▉";
					} else if (res < 0.5) {
						if (res >= 0.25) cout << "▓";
						if (res < 0.25) cout << "▓";
					}
				} else if (res <= 0) {
					if (res <= -.5) {
						if (res <= -.75) cout << "░";
						if (res > -.75) cout << "░";
					} else if (res > -.5) {
						if (res <= -.25) cout << "▒";
						if (res > -.25) cout << "▒";						
					}					
				}
				//printf("%4.4f ",x3[i + sqrt_t*j]);
			}
			cout << endl;
		}
		cout << endl;

		x3[k] = 1;
		for (int i = 0; i < n; i++) {
			f_xw2[i] = W3(i,W2.w); //bias
			for (int j = 0; j < W2.w; j++) {
				if (dist(j,i,sqrtn, sqrtk)) { 
					f_xw2[i] += W2(i,j)*x3[j];
				}
			}
			//f_xw2[i] =  dot(W2,i,x3);
			x2[i] = f2(f_xw2[i]);
			f_xw2[i] = f2_(f_xw2[i]);	
		}// cerr << "3";
		//x2[1] = 1;
		x2[n] = 1;

		cout << endl;
		for (int i = 0; i < sqrt(x2.n); i++) {
			for (int j = 0; j < sqrt(x2.n); j++) {
				double res = x2[i + sqrt(x2.n)*j];
				if (res > 0) {
					if (res > 0.5) {
						if (res > 0.75) cout << "█";
						if (res < 0.75) cout << "▉";
					} else if (res < 0.5) {
						if (res > 0.25) cout << "▓";
						if (res < 0.25) cout << "▓";
					}
				} else if (res < 0) {
					if (res < -.5) {
						if (res < -.75) cout << "░";
						if (res > -.75) cout << "░";
					} else if (res > -.5) {
						if (res < -.25) cout << "▒";
						if (res > -.25) cout << "▒";						
					}					
				}
				//printf("%4.4f ",x3[i + sqrt_t*j]);
			}
			cout << endl;
		}
		cout << endl;


		for (int i = 0; i < m; i++) {
			f_xw1[i] = dot(W1,i,x2);
			x1[i] = f1(f_xw1[i]);
			f_xw1[i] = f1_(f_xw1[i]);
		}
 //cerr << "1";
		//cout << "x1 = " << endl;

		/*double cumloss = (loss+loss1+loss2) / 3.;
		double D = (abs(loss1-loss2) + abs(loss1 - loss))/cumloss;
		loss2 = loss1;
		loss1 = loss;
		loss = 0;
		*/
		//loss2 = 0;
		if ((1-loss1)*100 >= 97 && N >= 2000) { 
			delta = 0.02;
		}
		for (int i = 0; i < m; i++) {
			cout << x1[i] << endl;
			loss2 += (int)((x1[i]>0?1:-1) != y0[i]);
		}
		if (!(N%100)) {
			loss1 = loss2/100;
			loss2 = 0;
		} 



		cout <<endl << "ACC = " << (1-loss1)*100 << "%" << endl;
		//cout <<endl << "cumloss = " << cumloss;

		//cout <<endl << "deltaD = " << D;
		cout<< endl;
/*
		if (D < 1 || cumloss > 10) {
			//delta *= 1.4;
		} else	
		if (D > 2.5 || cumloss < 0.5) {
			//delta /= 1.4;
		}*/
		/*double w1 = W1.weight();
		double w2 = W2.weight();
		double w3 = W3.weight();
		double l1 = f(w1);
		double l2 = f(w2);
		double l3 = f(w3);*/
		//cout << "delta = "<< endl;
 //cerr << "a"; W2(0,0);
		/*for (int i = 0; i < m; i++) {
			W1(i) -= delta*(f1_(dot(W1,i,x2))*x2*ced(x1[i],y0[i])) + -1*l*W1(i);
		}	

		for (int i = 0; i < n+1; i++) {
			for (int j = 0; j < m; j++) {
				W2(i) -= delta*f2_(dot(W2,i,x3))*W1(j,i)
							  *f1_(dot(W1,j,x2))*x3*ced(x1[j],y0[j]);
			}
			W2(i) -= l*W2(i);
		}*/

		for (int i = 0; i < W1.h; i++) {
			for (int j = 0; j <= W1.w; j++) {
				W1(i,j) -= l*W1(i,j);
				W1(i,j) -= delta*ced(x1[i],y0[i])
							 	*f_xw1[i]*x2[j];
			}
		}
	//cerr << "c";
		for (int i = 0; i < W2.h; i++) {
			for (int j = 0; j <= W2.w; j++) {
				if (dist(j,i,sqrtn, sqrtk) || j == W2.w) {
					double f_xw2i_x3j = f_xw2[i]*x3[j];
					W2(i,j) -= l*W2(i,j);
					for (int k = 0; k < W1.h; k++){
						W2(i,j) -= delta*f_xw2i_x3j
										*(W1p(k,i)=f_xw1[k]*W1(k,i))
										*ced(x1[k],y0[k]);
					}
				}
			}
		}


		for (int i = 0; i < W3.h; i++) {
						///cerr << "Xw3 = " <<  f_xw3[i]<< endl;
		for (int j = 0; j <= W3.w; j++) {
				if (dist(j,i,sqrtk, sqrtt) || j == W3.w) {
					double f_xw3i_x4j = f_xw3[i]*x4[j];
					W3(i,j) -= l*W3(i,j);
					for (int k = 0; k < W2.h; k++){
					for (int n = 0; n < W1.h; n++)
						W3(i,j) -= delta*f_xw3i_x4j
										*(W2p(k,i) = f_xw2[k]*W2(k,i))
										*W1p(n,k)
										*ced(x1[n],y0[n]) ;
					}
				}
			}
		}

		for (int i = 0; i < W4.h; i++) {
						///cerr << "Xw3 = " <<  f_xw3[i]<< endl;
		for (int j = 0; j <= W4.w; j++) {
				if (dist(j,i,sqrtt, sqrtr) || j == W4.w) {
					double f_xw4i_x5j = f_xw4[i]*x5[j];
					W4(i,j) -= l*W4(i,j);
					for (int t = 0; t < W3.h; t++) {
						double f_xw4i_x5j_W3ti = 
									f_xw4i_x5j*f_xw3[t]*W3(t,i);
					for (int k = 0; k < W2.h; k++){
					for (int n = 0; n < W1.h; n++)
						W4(i,j) -= delta*f_xw4i_x5j_W3ti
										*W2p(k,t)
										*W1p(n,k)
										*ced(x1[n],y0[n]) ;
					}}
				}
			}
		}




	//W1.print();
	//W2.print();
	//W3.print();

/*
		W2 -= delta* mul(app(ced,x1,y0),
					 mul(app(f1_,W1*x2),W1
					*mul(app(f2_,W2*x3),Matrix(W2.h,x3)))) + l*W2;
		W3 -= delta* mul(app(ced,x1,y0),
					 mul(app(f1_,W1*x2),W1
					*mul(app(f2_,W2*x3),W2
					*mul(app(f3_,W3*x4),Matrix(W3.h,x4))))) + l*W3;
*/
 //cerr << "b";

	//cerr << "C";
	} //while (1);


////////////////////////////////////////////
	int x = 0;
while (0) {
	x++;
	if (x >= x4.n) {
		x = 0;
	}
	cout <<endl << "X == " << x << endl;
		int res;
		x4 = Vector(x4.n); x4[x] = 1;
		//Gen_Ex(x4,sqrt_t,&res);
		y0[0] = res;
		x4[t] = 1;

		for (int i = 0; i < k; i++) {
			f_xw3[i] = dot(W3,i,x4);
			x3[i] = f3(f_xw3[i]);
			/// cerr << x3[i];

		} // cerr << endl << endl; dot(W3,0,x4);

		cout << endl;
		for (int i = 0; i < sqrt_t; i++) {
			for (int j = 0; j < sqrt_t; j++) {
				double res = x3[i + sqrt_t*j];
				if (res > 0) {
					if (res > 0.5) {
						if (res > 0.75) cout << "█";
						if (res < 0.75) cout << "▉";
					} else if (res < 0.5) {
						if (res > 0.25) cout << "▓";
						if (res < 0.25) cout << "▓";
					}
				} else if (res < 0) {
					if (res < -.5) {
						if (res < -.75) cout << "░";
						if (res > -.75) cout << "░";
					} else if (res > -.5) {
						if (res < -.25) cout << "▒";
						if (res > -.25) cout << "▒";						
					}					
				}
				//printf("%4.4f ",x3[i + sqrt_t*j]);
			}
			cout << endl;
		}
		cout << endl;

		x3[k] = 1;
		for (int i = 0; i < n; i++) {
			f_xw2[i] =  dot(W2,i,x3);
			x2[i] = f2(f_xw2[i]);	
		}// cerr << "3";
		x2[1] = 1;
		x2[n] = 0;
		for (int i = 0; i < m; i++) {
			f_xw1[i] = dot(W1,i,x2);
			x1[i] = f1(f_xw1[i]);
		}
		for (int i = 0; i < m; i++) {
			cout << x1[i] << " ";
			loss += logreg(x1[i],y0[i]);
		}
}

	return 0;
}